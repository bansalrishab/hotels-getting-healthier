Responding to increasing demands by guests, hotels around the country are taking a number of steps to make their establishments a healthier place to stay.

The American Hotel & Lodging Association (AH&LA) recently conducted its 2010 Lodging Survey, polling 9,000 hotels for a detailed analysis of health trends in today's hotels. The large sample size and the sophisticated analytic breakdown by market segment made this the largest representative sampling of trends in the American lodging industry.

The survey revealed that the number of hotels with gym equipment in rooms nearly tripled to 17 percent, from 6 percent in 2008. It also found that 83 percent have an exercise room/health and fitness facility, up from 63 percent in 2004; that 56 percent have non-smoking rooms, an 18 point increase from 2008; and that 38 percent make allergy free rooms available, up from 24 percent.

Those looking for a healthier stay will find the following hotel chains to their liking:

Fairmont offers on-site facilities as well as affiliations with top gyms. Fairmont's signature Willow Stream spas include healing mineral pools, relaxing lounges, and high tech workout rooms,

The Four Seasons offers a complete personal oasis of private spa rooms, relaxing massages , often in the quiet convenience of your guest room.

Kimpton's innovative urban boutique chain �thinks outside the box� when it comes to healthy lifestyle. Their in-room mind/body/spa includes massages with organic products, complimentary 24-hour On-Demand Pilates TV channels, and on-site workout facilities.

[Hotel st gallen](http://www.oberwaid.ch/) represents the pinnacle of luxury, the epicenter of comfort and of course, the optimal place for relaxation, as guests searching for hotels in st gallen enjoy a wide array of amenities and exquisite features.

On a personal note, it's very convenient to stay at a hotel that has on-site fitness facilities. When you're out of town, the last thing you want to do is drive around an unfamiliar city looking for fitness gyms. And too many are poorly run and smelly.

